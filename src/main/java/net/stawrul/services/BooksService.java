package net.stawrul.services;

import net.stawrul.model.Book;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Komponent (serwis) biznesowy do realizacji operacji na książkach.
 */
@Service
public class BooksService extends EntityService<Book> {

    //Instancja klasy EntityManger zostanie dostarczona przez framework Spring
    //(wstrzykiwanie zależności przez konstruktor).
    public BooksService(EntityManager em) {

        //Book.class - klasa encyjna, na której będą wykonywane operacje
        //Book::getId - metoda klasy encyjnej do pobierania klucza głównego
        super(em, Book.class, Book::getId);
    }

    /**
     * Pobranie wszystkich książek z bazy danych.
     *
     * @return lista książek
     */
    public List<Book> findAll() {
        //pobranie listy wszystkich książek za pomocą zapytania nazwanego (ang. named query)
        //zapytanie jest zdefiniowane w klasie Book
        return em.createNamedQuery(Book.FIND_ALL, Book.class).getResultList();
    }

    /**
     * Wyszukiwanie książek, o liczbie stron większej niż podana
     * @param books Lista książek, które przeszukujemy
     * @param x graniczna liczba stron
     * @return Lista książek o liczbie stron większej od x
     */
    public List<Book> PageNumbersGreaterThan(List<Book> books,Integer x)
    {
        List<Book> result = new ArrayList<>();

        for (Book book : books)
            if(book.getPages_number() > x)
                result.add(book);

        return result;
    }
    
    public List<Book> findByNameFragment(String fragment) {
        List<Book> result = new ArrayList<>();

        for (Book book : findAll()) {
            if(book.getTitle().contains(fragment)) {
                result.add(book);
            }
        }

        return result;
    }

}

