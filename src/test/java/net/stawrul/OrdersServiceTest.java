package net.stawrul;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.OrdersService;
import net.stawrul.services.exceptions.NoOrderElementsException;
import net.stawrul.services.exceptions.OutOfStockException;
import net.stawrul.services.exceptions.TooCheapException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {

    @Mock
    EntityManager em;

    @Test(expected = OutOfStockException.class)
    public void whenOrderedBookNotAvailable_placeOrderThrowsOutOfStockEx() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(0);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    @Test(expected = OutOfStockException.class)
    public void whenOrderedMoreBooksThatsAvailable_placeOrderThrowsOutOfStockEx() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        book.setPrice(50);
        order.getBooks().add(book);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    @Test
    public void whenOrdereBooksThatsAvailable_SaveOrderOnce() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(2);
        book.setPrice(50);
        order.getBooks().add(book);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }

    @Test(expected = TooCheapException.class)
    public void whenOrderedBookTotalPriceIsTooLow_placeOrderThrowsTooCheapException() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        book.setPrice(49);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    @Test
    public void whenOrderedBookTotalPriceIsEnough_SaveOrderOnce() {
        //Arrange
        Order order = new Order();
        Book book1 = new Book();
        Book book2 = new Book();
        Book book3 = new Book();
        book1.setAmount(1);
        book1.setPrice(20);
        book2.setAmount(1);
        book2.setPrice(20);
        book3.setAmount(1);
        book3.setPrice(10);


        order.getBooks().add(book1);
        order.getBooks().add(book2);
        order.getBooks().add(book3);

        Mockito.when(em.find(Book.class, book1.getId())).thenReturn(book1);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);
        Mockito.when(em.find(Book.class, book3.getId())).thenReturn(book3);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }

    @Test(expected = NoOrderElementsException.class)
    public void whenOrderContainNoBooks_placeOrderThrowsNoOrderElementsException() {
        //Arrange
        Order order = new Order();
        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    @Test(expected = NoOrderElementsException.class)
    public void whenOrderIsNotPresent_placeOrderThrowsNoOrderElementsException() {
        //Arrange
        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(null);

        //Assert - exception expected
    }

    @Test
    public void whenOrderedBookAvailable_placeOrderDecreasesAmountByOne() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        book.setPrice(60);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(0, (int)book.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }

}
